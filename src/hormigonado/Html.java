/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hormigonado;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Fran
 */
public class Html {
    BufferedWriter bw;
    final static String DEFAULTSTYLE = "\n\t<style>"
            + "\n\t\t\t body {}"
            + "\n\t\t\t pre {}"
            + "\n\t\t </style>";

    public Html(File f, String title) {
        try {
            FileWriter fw = new FileWriter(f);
            bw = new BufferedWriter(fw);
            
            // Initialize the html
            bw.write("\n<html>");
            bw.write("\n\t<head>");
            bw.write("\n\t\t<title>"+title+"</title>");
            bw.write(DEFAULTSTYLE);
            bw.write("\n\t</head>");
            bw.write("");
        } catch(IOException io) {
            io.printStackTrace();
            System.err.println("No se encuentra el archivo.");
        }
    }
    
    public void body () {
        try{
            bw.write("\n\t<body>");
        } catch (IOException io) {
            io.printStackTrace();
            System.err.println("No se encuentra el archivo.");
        }
    }
    
    public void line (String s) {
        try{
        bw.write("\n<pre>");
        bw.write(s);
        bw.write("\n</pre>");
        } catch (IOException io){
            io.printStackTrace();
            System.err.println("No se encuentra el archivo.");
        }
    }
    
    public void head (String s) {
        try{
        bw.write("\n<h1>"+s+"</h1>");
        } catch (IOException io){
            io.printStackTrace();
            System.err.println("No se encuentra el archivo.");
        }
    }
    
    public void close () {
        try{
            bw.write("</html>");
            bw.close();
        } catch (IOException io) {
            io.printStackTrace();
            System.err.println("No se encuentra el archivo.");
        }
    }
}
