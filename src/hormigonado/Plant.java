/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hormigonado;

/**
 *
 * @author Fran
 */
public class Plant {
    private double rigidez;
    private double weigth;
    private boolean fresh;
    private boolean cimbrado;

    public Plant(double rigidez, double weigth, boolean fresh, boolean cimbrado) 
    {
        this.rigidez = rigidez;
        this.weigth = weigth;
        this.fresh = fresh;
        this.cimbrado = cimbrado;
    }
    
    public Plant (double rigidez, double weigth) 
    {
        this(rigidez,weigth,false,false);
    }
    
    public Plant () {
        this(1,1,false,false);
    }

    public double getRigidez() {
        return rigidez;
    }

    public double getWeigth() {
        return weigth;
    }

    public boolean isFresh() {
        return fresh;
    }

    public boolean isCimbrado() {
        return cimbrado;
    }

    public void setRigidez(double inercia) {
        this.rigidez = inercia;
    }

    public void setWeigth(double weigth) {
        this.weigth = weigth;
    }

    public void setFresh(boolean fresh) {
        this.fresh = fresh;
    }

    public void setCimbrado(boolean cimbrado) {
        this.cimbrado = cimbrado;
    }
    
    

    // Check
    public static void main(String[] args) {
        // TODO code application logic here
    }
    
}
