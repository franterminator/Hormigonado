/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hormigonado;

/**
 *
 * @author Fran
 */
public class Test {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // Initialize plants
        Plant[] plants = new Plant[5];
        for(int i=0; i<plants.length; i++)
            plants[i] = new Plant();
        // Initializa build
        Build bd = new Build(plants,2);
        
        // Print test
        try{
        System.out.println("***Construccion");
        System.out.println(bd.toString());
        
        System.out.println("***Nueva planta");
        bd.newPlant();
        System.out.println(bd.toString());
        
        System.out.println("***Nueva planta");
        bd.newPlant();
        System.out.println(bd.toString());
        
        System.out.println("***Descimbrado");
        bd.desCimbrar();
        System.out.println(bd.toString());
        
        System.out.println("***Nueva planta");
        bd.newPlant();
        System.out.println(bd.toString());
        
        System.out.println("***Descimbrado");
        bd.desCimbrar();
        System.out.println(bd.toString());

        } catch(Exception e){
            e.printStackTrace();
        }
    }
    
}
