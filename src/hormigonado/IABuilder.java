/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hormigonado;

/**
 *
 * @author Fran
 */
public class IABuilder {
    int kindOfText;
    Html html;
    Build bd;
    int nPlants;

    public IABuilder(int kindOfText, Html html, Build bd) {
        this.kindOfText = kindOfText;
        this.html = html;
        this.bd = bd;
        nPlants = bd.getPlants().length;
    }
    public void buildingSanitario () {
        try {
            bd.forjadoSanitario();
        } catch (Exception e) {
                e.printStackTrace();        
        }
    }
    
    public void building() {
        // 5 Construct new Plant and then descimbrar
        contruir: for(int i=0; ; i++){
            if (kindOfText==0) html.line(bd.toFigures());
            else html.line(bd.toHtml());
            System.out.println(bd.toString());
            try{
                // LLegamos a la ultima planta
                if(bd.getCurrentPlant()>=(nPlants-1)){
                    if(bd.getLastCimbra()==bd.getCurrentPlant())
                        break contruir;
                    else {
                        bd.desCimbrar();
                        html.head("Descimbramos la planta "+bd.getLastCimbra());
                    }
                }
                // En el resto de plantas
                // Si sobran cimbras, construimos
                else if(bd.getCimbras()>0){
                    bd.newPlant();
                    html.head("Construimos la planta "+bd.getCurrentPlant());
                }
                else {
                    bd.desCimbrar();
                    html.head("Descimbramos la planta "+bd.getLastCimbra());
                } 
            } catch (Exception e) {
                e.printStackTrace();        
            }
        }
    }
}
