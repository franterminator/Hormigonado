/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hormigonado;

/**
 *
 * @author Fran
 */
public class Build {
    Plant[] plants;
    int nBuilded;
    int limitCimbras;
    int cimbras;
    int lastCimbra;
    int floor;  //the index of the floor
    double[] pilar;
    double[] puntal;
    double suelo;
    
    public Build (Plant[] plants, int nCimbras) {
        // Cimbras
        this.limitCimbras = nCimbras;
        this.cimbras = nCimbras;
        
        // Iniciación de los vectores
        /* Se le suma 1 para que cuando se contruyan todas las plantas simule 
           una planta por arriba ficticia de valor 0.
        */ 
        pilar = new double [plants.length+1];
        puntal = new double [plants.length+1];
        this.plants = plants;
        
        // Construimos el suelo
        nBuilded=0;
        lastCimbra=0;
        plants[0].setWeigth(0.);
        pilar[floor] = 0.;
        puntal[floor] = 0.;
    }
    
    public int getCimbras() {
        return cimbras;
    }
    
    public int getCurrentPlant() {
        return nBuilded;
    }

    public int getLastCimbra() {
        return lastCimbra;
    }
    
    public Plant[] getPlants() {
        return plants;
    }
    
    public void forjadoSanitario () throws Exception {
        // Añadimos una planta mas
        if(++nBuilded>=plants.length)
            throw new Exception("NO se puede construir mas.");
        
        // Ultima planta construida
        plants[nBuilded].setCimbrado(false);
        plants[nBuilded].setFresh(false);
        pilar[nBuilded] = plants[nBuilded].getWeigth();
        puntal[nBuilded] = 0.;
        floor = 1;
    }
    
    public void newPlant () throws Exception {
        // Añadimos una planta mas
        if(++nBuilded>=plants.length)
            throw new Exception("NO se puede construir mas.");

        
        // Ultima planta construida
        plants[nBuilded].setCimbrado(true);
        plants[nBuilded].setFresh(true);
        pilar[nBuilded] = 0.;
        puntal[nBuilded] = plants[nBuilded].getWeigth();
        desFresh();
        if(cimbras>=0)--cimbras;
        else throw new Exception("NO se puede cimbrar.");
        
        // Ultima planta es el suelo
        if(lastCimbra==floor) {
            pilar[floor]+= plants[nBuilded].getWeigth();
            return;
        }
        
        // Calculamos el resto de plantas
        double sum = sumCimbrados();
        
        sum = plants[nBuilded].getWeigth()/sum;
        for(int i=nBuilded-1; i>=lastCimbra; i--){
            pilar[i]+=sum*plants[i].getRigidez();
            puntal[i]=puntal[i+1]+plants[i].getWeigth()-pilar[i];
        }
        
        System.out.println("***Construimos la planta "+nBuilded);
    }
    
    public void desCimbrar () throws Exception {
        // Calculamos los coeficientes
        double sum = sumCimbrados();
        sum = (pilar[lastCimbra]-plants[lastCimbra].getWeigth())/sum;
        
        // Quitamos la ultima cimbra
        pilar[lastCimbra] = plants[lastCimbra].getWeigth();
        puntal[lastCimbra] = 0.;
        ++lastCimbra;
        
        // Ultima planta construida
        plants[lastCimbra].setCimbrado(false);
        plants[nBuilded].setFresh(false);
        
        // Calculamos el resto de plantas
        for(int i=nBuilded; i>=lastCimbra; i--){
            pilar[i]+=sum*plants[i].getRigidez();
            puntal[i]=puntal[i+1]+plants[i].getWeigth()-pilar[i];
        }

        if(!plants[nBuilded].isFresh())++cimbras;
        else throw new Exception("NO se puede descimbrar.");
        
        System.out.println("***Descimbramos la planta "+lastCimbra);
    }
    
    public double sumCimbrados(){
        double sum=0;
        
        for(Plant plant:plants){
            if(plant.isCimbrado()) sum+=plant.getRigidez();
        }
        return sum;
    }
    
    private void desFresh() { 
        if(plants[nBuilded-1].isFresh())plants[nBuilded-1].setFresh(false);
    }
    
    @Override
    public String toString () {
        StringBuilder str = new StringBuilder();
        str.append("--------------------------------");
        for(int i=0; i<=nBuilded; i++){
            if(lastCimbra==0 && i==0) {
                str.append("\nSuelo: </br>");
                str.append("\n-> Pilar: ").append(pilar[i]);
            }
            else if (i==0)
                continue;
            else {
                str.append("\nPlant: ").append(i);
                str.append("\n-> is Cimbrado? ").append(plants[i].isCimbrado());
                str.append("\n-> is Fresh? ").append(plants[i].isFresh());
                str.append("\n-> Weigth: ").append(String.format("%5.5f", plants[i].getWeigth()));
                str.append("\n-> Pilar: ").append(String.format("%5.5f",pilar[i]));
                str.append("\n-> Puntal: ").append(String.format("%5.5f",puntal[i]));
                
            }
            str.append("\n--------------------------------");
        }
        return str.toString();
    }

    public String toHtml () {
        StringBuilder str = new StringBuilder();
        str.append("--------------------------------</br>");
        for(int i=0; i<=nBuilded; i++){
            if(lastCimbra==0 && i==0) {
                str.append("Suelo: </br>");
                str.append("-> Pilar: ").append(pilar[i]).append("</br>");
            }
            else if (i==0)
                continue;
            else {
                str.append("Plant: ").append(i).append("</br>");
                str.append("-> is Cimbrado? ").append(plants[i].isCimbrado()).append("</br>");
                str.append("-> is Fresh? ").append(plants[i].isFresh()).append("</br>");
                str.append("-> Weigth: ").append(String.format("%5.5f", plants[i].getWeigth())).append("</br>");
                str.append("-> Pilar: ").append(String.format("%5.5f",pilar[i])).append("</br>");
                str.append("-> Puntal: ").append(String.format("%5.5f",puntal[i])).append("</br>");
                
            }
            str.append("--------------------------------").append("</br>");
        }
        return str.toString();
    }
    
    public String toFigures () {
        StringBuilder str = new StringBuilder();
        for(int i=nBuilded; i>=0; i--){
            if(i==0) {
                String cimbra = plants[i+1].isCimbrado()? "x" : " ";
                str.append(String.format("\n %7s |%s     %s| &rarr; %5.3f",
                        "",cimbra,cimbra,puntal[i+1]));
                str.append("\n__________________________");
                if(lastCimbra==0)
                    str.append(String.format("\n%3.3f",pilar[i]));
            }
            else if (i==nBuilded) {
                String piso = plants[i].isFresh()? "*********" : "---------";
                str.append(String.format("\n%10s %3.3f","",plants[i].getWeigth()));
                str.append(String.format("\n %3.3f &larr; %s",pilar[i],piso));
            }
            else {
                String piso = plants[i].isFresh()? "*********" : "---------";
                String cimbra = plants[i+1].isCimbrado()? "x" : " ";  
                str.append(String.format("\n %7s |%s%3.3f%s| &rarr; %5.3f",
                        "",cimbra,plants[i].getWeigth(),cimbra,puntal[i+1]));
                str.append(String.format("\n %3.3f &larr; %s",pilar[i],piso));        
            }

        }
        return str.toString();
    }

    

}