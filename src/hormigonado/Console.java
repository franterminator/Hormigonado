/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hormigonado;

import java.io.File;
import java.util.Scanner;

/**
 *
 * @author Fran
 */
public class Console {
    // Methods for printing
    /**
     * Print a line with * in the cmd
     */
    public static void line(){
        int length = 50;
        for(int i=0; i<length;i++)
            System.out.printf("*");
        System.out.println("");
    }
    /**
     * Print a line in the cmd with the sign you want.
     * @param texto 
     */
    public static void line(String texto){
        int length = 50;
        for(int i=0; i<length;i++)
            System.out.printf(texto);
        System.out.println("");
    }
    /**
     * Print a line in the cmd with the sign you want length times.
     * @param texto
     * @param length 
     */
    public static void line(String texto, int length){
        for(int i=0; i<length;i++)
            System.out.printf(texto);
        System.out.println("");
    }
    /**
     * A printf for strings.
     * @param texto 
     */
    public static void print(String texto){
        System.out.printf("%s%n",texto);
    }
    /**
     * Print a menu header centered.
     * @param texto 
     */
    public static void printHeader (String texto){
        int media = (int)texto.length()/2;
        line("*",30);
        System.out.printf("%s %13s%-13s %s%n","|",texto.substring(0, media),texto.substring(media),"|");
        line("*",30);
    }
    /**
     * Print a menu with diferents options, automatically add the numbers of the
     * menu.
     * @param options 
     */
    public static void printMenu (String[] options){
        for(int i=0;i<options.length;i++){
            System.out.printf("%d) %s%n",i+1,options[i]);
        }
    }
    /**
     * Clear the cmd.
     */
    public static void cls(){
        for(int clear = 0; clear < 500; clear++)
        {
            System.out.println(" ");
            
        }
        for(int clear = 0; clear < 500; clear++)
        {
            System.out.println("\b");
            
        }
    }
    
    public static int result () {
        Scanner input = new Scanner (System.in);
        int sel = input.nextInt();
        return sel;
    }
    
    // Menus
    public static int intro () {
        line("~");
        print(" *    *     ******      ");
        print(" *    *     *    *      ");
        print(" ******     *    *    ***   *****   ***  *-*   ***   *   *");
        print(" *    *     *    *    *     * * *    *   *-*   * *   * * *");
        print(" *    *     ******    *     * * *   ***    *   ***   *   *");
        print("                                         ***");
        line("~");
        
        String[] menu = {"Comenzar","Salir"};
        printHeader("INTRO");
        printMenu(menu);
        int sel = result();
        switch(sel){
            case 1:
                return 0;
            case 2:
                System.exit(0);
        }
        return 1;
    }
    
    
    public static void main (String args[]) {
        // 1 Initialize the intro menu
        intro();
        // 2 Number of plants
        line("-");
        print("Introduzca el nº de plantas");
        int nPlants = result()+1; // + 1 porque el suelo es una planta
        Plant plants[] = new Plant[nPlants];
        for(int i=0; i<nPlants; i++)
            plants[i] = new Plant();
        // 3 Initialize the build object
        line("-");
        print("Introduzca el nº de cimbras");
        int nCimbras = result();
        Build bd = new Build(plants,nCimbras);
        
        // 4 Escoger archivo para imprimir resultados
        line("-");
        print("Quiere figuras o texto?\n1)Figuras \n2)Texto");
        int sel = result();

        File f = new File("result.html");
        
        Html html = new Html(f,"Resultados");
        html.body();


        // 5 Construct new Plant and then descimbrar
        contruir: for(int i=0; ; i++){
            if (sel==1) html.line(bd.toFigures());
            else html.line(bd.toHtml());
            System.out.println(bd.toString());
            try{
                // LLegamos a la ultima planta
                if(bd.getCurrentPlant()>=(nPlants-1)){
                    if(bd.getLastCimbra()==bd.getCurrentPlant())
                        break contruir;
                    else {
                        bd.desCimbrar();
                        html.head("Descimbramos la planta "+bd.getLastCimbra());
                    }
                }
                // En el resto de plantas
                // Si sobran cimbras, construimos
                else if(bd.getCimbras()>0){
                    bd.newPlant();
                    html.head("Construimos la planta "+bd.getCurrentPlant());
                }
                else {
                    bd.desCimbrar();
                    html.head("Descimbramos la planta "+bd.getLastCimbra());
                } 
            } catch (Exception e) {
                e.printStackTrace();        
            }
        }
        
        html.close();
        
    }
}
